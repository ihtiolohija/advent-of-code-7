package com.company;

import java.io.*;

public class Main {


    public static void main(String[] args) throws IOException {
        Controller controller = new Controller();
        //read file and pass the result to controller
        BufferedReader br = new BufferedReader(new FileReader("input.txt"));
        String line;
        while ((line = br.readLine()) != null) {
            controller.controlCommands(line);
        }
        controller.assembly();

        //store all the values to wires and check once more
        controller.printWires();
        controller.writeToFile(0);
//        int value = controller.wires.get("a");
//        controller.wires.clear();
//        controller.wires.put("b", value);
//        controller.writeToFile(222);
//        controller.assembly();
//        controller.printWires();
//        controller.writeToFile(1);
    }


    //TODO implement method for setting value to Wire (if command is null)
    //TODO implement method for recursively setting
}
