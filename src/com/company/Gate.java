package com.company;

import java.util.ArrayList;

/**
 * Created by Shur on 1/5/2016.
 */
public class Gate {
    ArrayList<Integer> out = new ArrayList<>();
    String name = null;
    ArrayList<Integer> input_signals = null;
    Integer result;

    public Gate(String type) {
        this.input_signals = new ArrayList<>(2);
        this.result = null;
        this.name = type;
    }

    public String getName() {
        return this.name;
    }

    public void setSignal(Integer signal) {
        this.input_signals.add(signal);
    }

    public Integer createBitwiseGate() {
        if (this.name.equals("AND")) {
            return this.and();
        }
        if (this.name.equals("OR")) {
            return this.or();
        }

        return null;
    }

    public Integer createBitshiftGate() {
        if (this.name.equals("LSHIFT"))
            return this.lShift();
        if (this.name.equals("RSHIFT"))
            return this.rShift();
        return null;
    }

    public Integer complement() {
//        int result = ~this.input_signals.get(0);
        int result = this.input_signals.get(0) ^ 65535;
        System.out.format("Gate is " + this.getName() + ", signals are : %s", this.input_signals.get(0) + ", result is " + result + "\n");
        return this.result = result;
    }

    public Integer lShift() {
        this.printSignals();
        int result = this.input_signals.get(0) << this.input_signals.get(1);
        System.out.format("Gate is " + this.getName() + ", signals are : %s, %s", this.input_signals.get(0),
                this.input_signals.get(1) + ", result is " + result + "\n");
        return this.result = result;
    }

    public Integer rShift() {
        int result = this.input_signals.get(0) >> this.input_signals.get(1);
        System.out.format("Gate is " + this.getName() + ", signals are : %s, %s", this.input_signals.get(0),
                this.input_signals.get(1) + ", result is " + result + "\n");
        return this.result = result;
    }


    public Integer and() {
        int result = this.input_signals.get(0) & this.input_signals.get(1);
        System.out.format("Gate is " + this.getName() + ", signals are : %s, %s", this.input_signals.get(0),
                this.input_signals.get(1) + ", result is " + result + "\n");
        return this.result = result;
    }

    public Integer or() {
        int result = this.input_signals.get(0) | this.input_signals.get(1);
        System.out.format("Gate is " + this.getName() + ", signals are : %s, %s", this.input_signals.get(0),
                this.input_signals.get(1) + ", result is " + result + "\n");
        this.result = result;
        return this.result;
    }

    private void printSignals() {
        for (Integer i : this.input_signals) {
            System.out.println("Gate is " + this.getName() + "\n signal is" + i);
        }
    }
}
