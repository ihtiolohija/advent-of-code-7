package com.company;

import java.io.*;
import java.util.*;

/**
 * Created by Shur on 1/5/2016.
 */
public class Controller {
    //TODO Make logger class for console
    //TODO CHECK UP THE #1 part one
    String input = null;
    String output = null;

    Gate gate = null;
    String current_instruction = null;
    //define global hashmap for wires storage
    HashMap<String, Integer> wires = new HashMap<>();

    HashSet<String> instructions = new HashSet<>();

    public void controlCommands(String command) {
        this.instructions.add(command);
    }

    public void assembly() {
        for (int i = 0; i < this.instructions.size(); i++) {
            for (String c : this.instructions) {
//                this.current_instruction = c;
                System.out.println("instruction is " + c);
                this.readCommands(c);
            }
        }
    }

    public void readCommands(String command) {
        //Mock test cases
        //String command = "NOT hj -> hk";
        // String command = "14146 -> b";
        //String command = "bn OR by -> bz";
        //parse command;
        //get Wires value from command, apply Gate and write result to output;

        String[] parsed = command.split("->");
        this.input = parsed[0].trim();
        this.output = parsed[1].trim();
        this.processSplitted();
    }

    public void processSplitted() {
        //make decision based on the command's name
        Gate current_gate = getGate(this.input);
        //parse command
        String[] str = this.input.split(" ");
        if (current_gate == null) {
            //assigning value
            Integer value = getNumber(str[0]);
            if (value != null)
                this.wires.put(this.output, value);
            else {
                if (this.wires.containsKey(str[0]))
                    this.wires.put(this.output, this.wires.get(str[0]));
            }
        }
        //if command is NOT (first element), provide first element's signal to Gate
        else {
            if (current_gate.getName().equals("NOT")) {
                if (this.wires.containsKey(str[1])) {
                    current_gate.setSignal(this.wires.get(str[1]));
                    this.wires.put(this.output, current_gate.complement());
                    System.out.println("NOT, wire " + str[1] + " pushed to " + this.output + " :" + this.wires.get(this.output));
                }
            }
            //process the BitShift gates
            if (current_gate.getName().equals("RSHIFT") || current_gate.getName().equals("LSHIFT")) {
                if (this.wires.containsKey(str[0])) {
                    current_gate.setSignal(this.wires.get(str[0]));
                    current_gate.setSignal(Integer.parseInt(str[2]));
                    this.wires.put(this.output, current_gate.createBitshiftGate());
                    System.out.println("BitShift, wire " + str[0] + ", " + str[1] + " pushed to " + this.output + " :" + this.wires.get(this.output));

                }
            }
            //process AND and OR gates
            if (current_gate.getName().equals("AND") || current_gate.getName().equals("OR")) {
                String s1 = str[0];
                String s2 = str[2];
                for (int i = 0; i < str.length; i += 2) {
                    if (getNumber(str[i]) != null) {
                        current_gate.setSignal(Integer.parseInt(str[i]));
                        if (str[i].equals(s1) && this.wires.containsKey(s2)) {
                            current_gate.setSignal(this.wires.get(s2));
                            this.wires.put(this.output, current_gate.createBitwiseGate());
                        }
                        if (str[i].equals(s2) && this.wires.containsKey(s1)) {
                            current_gate.setSignal(this.wires.get(s1));
                        }
                    }
                }
                if (this.wires.containsKey(s1) && this.wires.containsKey(s2)) {
                    current_gate.setSignal(this.wires.get(s1));
                    current_gate.setSignal(this.wires.get(s2));
                    this.wires.put(this.output, current_gate.createBitwiseGate());
//                    this.instructions.remove(this.current_instruction);
                    System.out.println("Bitwise, wire " + s1 + ", " + s2 + " pushed to " + this.output + " :" + this.wires.get(this.output));
                }
            }
        }
    }

    public Gate getGate(String command) {

        String[] values = command.split("[^A-Z]+");
        if (values.length == 0)
            return null;
        for (String value : values) {
            if (!value.isEmpty()) {
                gate = new Gate(value);
            }
        }
        return gate;
    }


    public Integer getNumber(String command) {
        String[] values = command.split("[^0-9]");
        Integer result = null;
        //use Character.isDigit(chrs) instead of regex

        for (String value : values)
            if (!value.isEmpty())
                result = Integer.parseInt(value);
        return result;
    }

    public void printWires() {
        if (!this.wires.isEmpty()) {
            for (String s : this.wires.keySet()) {
                System.out.println("Wire " + s + ", value: " + this.wires.get(s));
            }
        } else
            System.out.println("Collection is empty");
    }

    public void writeToFile(int i) {
        if (!this.wires.isEmpty()) {
            try (Writer writer = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream("output" + i + ".txt"), "utf-8"))) {
                for (String s : this.wires.keySet()) {
                    writer.write("Wire " + s + ", value: " + this.wires.get(s) + "\n");
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
